/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.layout;

import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author Christian Cougourdan
 */
public class SubThemeHandlerInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubThemeHandlerInterceptor.class);

    private static final Pattern PATTERN_DOMAIN_EXTRACT = Pattern.compile("^.*\\.([^.]+)\\.[^.]+$");

    private static final Collection<String> ALLOWED_SUBTHEME = Arrays.asList("default", "guichet-entreprises", "guichet-partenaires", "guichet-qualifications");

    @Value("${ui.theme.defaultSubTheme:guichet-entreprises}")
    private String defaultSubTheme;

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
        final boolean res = super.preHandle(request, response, handler);

        if (handler instanceof HandlerMethod) {
            final String serverName = this.getServerName(request);
            LOGGER.debug("Detected request server name : {}", serverName);
            request.setAttribute("subTheme", this.getTemplate(serverName));
        }

        return res;
    }

    private String getServerName(final HttpServletRequest request) {
        final Enumeration<String> forwardedHosts = request.getHeaders("X-Forwarded-Host");
        if (forwardedHosts.hasMoreElements()) {
            return forwardedHosts.nextElement();
        } else {
            return request.getServerName();
        }
    }

    private String getTemplate(final String serverName) {
        return Optional.of(PATTERN_DOMAIN_EXTRACT.matcher(serverName)) //
                .filter(Matcher::matches) //
                .map(m -> m.group(1)) //
                .filter(ALLOWED_SUBTHEME::contains) //
                .orElse(this.defaultSubTheme);
    }

}
