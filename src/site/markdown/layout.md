## HEAD placeholders

Title can be customized with `title` placeholder.

Custom CSS and scripts can be inserted with two placeholders presents in `<head />` element :
* `links` used for stylesheets
* `scripts` used for custom javascripts


## BODY placeholders

![layout placeholders](images/Default Layout.draw.io.svg)
