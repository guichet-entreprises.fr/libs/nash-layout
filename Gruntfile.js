/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
'use strict';

var path = require('path');
var extend = require('util')._extend;

var project = {
        build: {
            directory: 'target',
            outputDirectory: 'target/classes',
            sourceDirectory: 'src/main/resources'
        }
};



module.exports = function (grunt) {

    var pkg = grunt.file.readJSON('package.json');

    grunt.initConfig({
        pkg: pkg,
        project: project,
        clean: {
            assets: [ '<%= project.build.sourceDirectory %>/public/theme/**/*', '!<%= project.build.sourceDirectory %>/public/theme/*/js', '!<%= project.build.sourceDirectory %>/public/theme/*/js/config.js' ],
            templates: [ '<%= project.build.sourceDirectory %>/views/theme/**/*' ]
        },
        copy: {
            assets: {
                files: [
                    {
                        expand: true,
                        cwd: 'node_modules/@guichet-entreprises/layout-assets',
                        src: [ '**/*', '!package.json' ],
                        dest: '<%= project.build.outputDirectory %>/public/theme/default'
                    }
                ]
            }
        },
        'string-replace': {
            templates: {
                options: {
                    replacements: [
                        {
                            pattern: /\$\{@appProperties\.staticBasePath\}/g,
                            replacement: '/theme/default/${subTheme}'
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        cwd: 'node_modules/@guichet-entreprises/layout-nash',
                        src: [ '**/*.html' ],
                        dest: '<%= project.build.outputDirectory %>/views/theme/default'
                    }
                ]
            }
        }
    });

    require('load-grunt-tasks')(grunt);
    grunt.registerTask('default', [ 'clean', 'copy', 'string-replace' ]);

};
