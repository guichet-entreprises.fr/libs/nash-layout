# General layout usage

## HEAD placeholders

Title can be customized with `title` placeholder.

Custom CSS and scripts can be inserted with two placeholders presents in HEAD element :
* `links` used for stylesheets
* `scripts` used for custom javascripts


## BODY placeholders

![layout placeholders](src/site/resources/images/Default Layout.draw.io.svg)
