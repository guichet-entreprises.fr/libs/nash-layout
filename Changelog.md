
# Changelog
Ce fichier contient les modifications techniques du module.

## [2.11.6.0] - 2019-07-03

- Projet : [Layouts](https://tools.projet-ge.fr/gitlab/libs/layouts)

###  Ajout

 - Ne plus afficher le menu "Connexion" si l'utilisateur est anonyme
 
  __Properties__ :

###  Modification

###  Suppression

